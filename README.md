# Eclass2Scrbl

Convert Gentoo Eclasses to Scribble documents.


## About

`eclass2scrbl` is a Racket tool to convert Gentoo Eclass documents
into Scribble documents.

A Scribble document can be converted to a number of formats:
HTML, Markdown, PDF, Tex, etc.


## License

Copyright (c) 2022, Maciej Barć xgqt@riseup.net

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
