MAKE            ?= make
RACKET          := racket
RACO            := raco
SCRIBBLE        := $(RACO) scribble

DO-DOCS         := --no-docs
EXE-FLAGS       := --orig-exe -v
INSTALL-FLAGS   := --auto --skip-installed $(DO-DOCS)
REMOVE-FLAGS    := --force --no-trash $(DO-DOCS)
DEPS-FLAGS      := --check-pkg-deps --unused-pkg-deps
SETUP-FLAGS     := --tidy --avoid-main $(DEPS-FLAGS)
TEST-FLAGS      := --heartbeat --no-run-if-absent --submodule test --table

PWD             ?= $(shell pwd)
BIN             := $(PWD)/bin

PKG              = eclass2scrbl


.PHONY: all
all: clean compile

.PHONY: clean
clean:
	find $(PWD) -type d -name "bin" -exec rm -dr {} +
	find $(PWD) -type d -name "compiled" -exec rm -dr {} +
	find $(PWD) -type d -name "doc" -exec rm -dr {} +

.PHONY: compile
compile:
	$(RACKET) -e "(require compiler/compiler setup/getinfo) (compile-directory-zos (path->complete-path \"$(PWD)\") (get-info/full \"$(PWD)/info.rkt\") #:skip-doc-sources? #t #:verbose #f)"

.PHONY: install
install:
	$(RACO) pkg install $(INSTALL-FLAGS) --name $(PKG)

.PHONY: setup
setup:
	$(RACO) setup $(SETUP-FLAGS) --pkgs $(PKG)

.PHONY: test
test:
	$(RACO) test $(TEST-FLAGS) --package $(PKG)

.PHONY: shellcheck
shellcheck:
	find $(PWD) -type f -name "*.sh" -exec shellcheck {} +

.PHONY: remove
remove:
	$(RACO) pkg remove $(REMOVE-FLAGS) $(PKG)

bin:
	mkdir -p $(BIN)
	$(RACO) exe $(EXE-FLAGS) -o $(BIN)/eclass2scrbl $(PWD)/eclass2scrbl/main.rkt
