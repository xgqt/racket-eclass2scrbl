;; This file is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.


#lang racket/base

(require (only-in racket/match match)
         (only-in racket/string string-join string-split string-trim)
         (only-in brag/support token)
         (only-in threading ~>>))

(provide (all-defined-out))


;; There is probably a better way to split on first element...

(define (split/first-colon str)
  (let ([splitted (string-split str ":")])
    (list (car splitted)
          (string-join (cdr splitted) ":"))))


(define (tokenize s)
  (let* ([lines (regexp-split "\n" s)]
         [len (length lines)])
    (for/list ([str lines]
               [n (in-range 1 (add1 len))])
      (match str
        [(regexp #rx"^# @(ECLASS|ECLASS_VARIABLE|FUNCTION|VARIABLE):")
         (~>> str
              (string-trim _ "# @")
              split/first-colon
              (map string-trim)
              (token 'HEADER _ #:line n))]
        [(regexp #rx"^# @CODE$")
         (token 'CODE-TAG #false #:line n)]
        [(regexp #rx"^# @SUBSECTION .*")
         (~>> str
              (string-trim _ "# @SUBSECTION ")
              (string-trim _ ":")
              (token 'SUB _ #:line n))]
        [(regexp #rx"^# @[A-Z_]+$")
         (~>> str
              (string-trim _ "# @")
              (token 'PROPERTY _ #:line n))]
        [(regexp #rx"^# @[A-Z_]+(|:) *$")  ; may include whitespaces at the end
         (~>> str
              (string-trim _ "# @")
              (string-trim _ ":")
              (token 'TAG-MULTI _ #:line n))]
        [(regexp #rx"^# @[A-Z_]+: .*$")
         (~>> str
              (string-trim _ "# @")
              split/first-colon
              (map string-trim)
              (token 'TAG-INLINE _ #:line n))]
        [(regexp #rx"^#.*")
         (~>> str
              (string-trim _ "#")
              (string-trim _ " ")  ; only one whitespace
              (token 'COMMENT _ #:line n))]
        [_
         (token 'CODE str #:line n #:skip? #true)]))))
